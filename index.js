const {Storage} = require("@google-cloud/storage");
const storage = new Storage();

exports.file_upload = async (req, res) =>
{
    res.set('Access-Control-Allow-Origin', '*');

    if (req.method === 'OPTIONS') 
    {
        // Send response to OPTIONS requests
        res.set('Access-Control-Allow-Methods', 'POST');
        res.set('Access-Control-Allow-Headers', 'Content-Type');
        res.set('Access-Control-Max-Age', '3600');
        res.status(204).send('');
    } else 
    {
        const body = req.body;
        const bucket = storage.bucket("smithey-file-bucket");
        const buffer = Buffer.from(body.content, "base64");
        const file = bucket.file(`${body.name}.${body.extension}`);
        await file.save(buffer);
        await file.makePublic();
        res.send({photoLink: file.publicUrl()});
    }
};